'use strict';

/* Controllers */
var bookApp = angular.module('bookApp', ['ngRoute', 'ngSanitize']);

/* Config */
bookApp.config([
  '$routeProvider', '$locationProvider',
  function($routeProvide, $locationProvider){
    /* Our structure */
    $routeProvide
        .when('/',{
          templateUrl:'template/home.html',
          controller:'BookListCtrl'
        })
        .when('/book/:bookId', {
          templateUrl:'template/book-detail.html',
          controller:'BookDetailCtrl'
        })
        .when('/about', {
          templateUrl:'template/about.html',
          controller:'AboutCtrl'
        })
        .when('/order/:bookId', {
          templateUrl:'template/order.html',
          controller:'OrderCtrl'
        })
        .otherwise({
          redirectTo: '/'
        });
  }
]);

/* About */
bookApp.controller('AboutCtrl', ['$scope', '$http', '$location',
  function($scope) {
    rerouting();
  }
]);

/* Book list */
bookApp.controller('BookListCtrl', ['$scope', '$http', '$location',
  function($scope, $http, $location) {
    $http.get('https://netology-fbb-store-api.herokuapp.com/book').success(function(data) {
      $scope.books = data;
      dragAndDrop();
      $scope.rowLimit = 4;
      $scope.showMore = function() {
        $scope.rowLimit += 4;
        if ($scope.rowLimit >= $scope.books.length) {
          $( 'button' ).addClass( 'hidden' );
        }
      }
      $( '#search' ).bind( "keyup change input", function () {
        $scope.rowLimit = $scope.books.length;
        $( 'button' ).addClass( 'hidden' );
      });
    });
  }
]);

/* Book detail */
bookApp.controller('BookDetailCtrl', ['$scope', '$http', '$location', '$routeParams',
  function($scope, $http, $location, $routeParams) {
    $scope.bookId = $routeParams.bookId;
    $http.get('https://netology-fbb-store-api.herokuapp.com/book/' + $routeParams.bookId).success(function(data) {
      $scope.book = data;
    });
    pupilMove();
    eyeBlink();
    rerouting();
  }
]);

/* Order */
bookApp.controller('OrderCtrl', ['$scope', '$http', '$location', '$routeParams',
  function($scope, $http, $location, $routeParams) {
    $scope.bookId = $routeParams.bookId;
    $http.get('https://netology-fbb-store-api.herokuapp.com/book/' + $routeParams.bookId).success(function(data) {
      $scope.book = data;
    });

    $http.get('https://netology-fbb-store-api.herokuapp.com/order/delivery').success(function(data) {
      $scope.deliveryMethods = data;
      $scope.changed = function() {
        if ($scope.selectedDelivery.needAdress) {
          $( '#postAddress' ).removeClass( 'hidden' );
          $( '#deliveryAddress' ).prop( 'required', true );
        } else {
          $( '#deliveryAddress' ).removeAttr( 'required' );
          $( '#postAddress' ).addClass( 'hidden' );
        }
        $( '#subtotal' ).text(Math.round((+$( 'input[name="delivery"]:checked' ).next().text() + $scope.book.price) * 100) / 100);
      }
      $scope.selectedDelivery = $scope.deliveryMethods[0]; // Checked default
      $scope.getPayment($scope.selectedDelivery.id); // Id transmission
    });

    $scope.getPayment = function(id) {
      $http.get('https://netology-fbb-store-api.herokuapp.com/order/delivery/' + id + '/payment').success(function(data) {
        $scope.paymentMethods = data;
        $scope.selectedPayment = $scope.paymentMethods[0]; // Checked default
      })
    }

    $http.get('https://netology-fbb-store-api.herokuapp.com/currency').success(function(data) {
      $scope.currencies = data;
      $scope.selectedCurrency = $scope.currencies[9]; // Selected default
      correctCurrencyName($scope.currencies); // Correct name of the currency
      $( '#subtotalCurrency' ).data( 'id', $scope.selectedCurrency.ID ); // For formData
      
      $scope.optionChanged = function() {
        var val = collectSubstring($scope.selectedCurrency, 0, $scope.selectedCurrency.indexOf('|')); // New value
        var id = collectSubstring($scope.selectedCurrency, $scope.selectedCurrency.indexOf('|') + 1, $scope.selectedCurrency.lastIndexOf('|'));
        var code = collectSubstring($scope.selectedCurrency, $scope.selectedCurrency.lastIndexOf('|') + 1); // New CharCode
        var prices = $( '.radio .price' ); // Span price of delivery
        var currencies = $( '.radio .currency' ); // Span currency of delivery
        var oldNameCurrency = currencies[0].textContent; // Past name currency
        var oldCurrency = $scope.currencies.find(function(obj) { // Past object currency
          return obj.CharCode == oldNameCurrency;
        });
        var oldVal = oldCurrency.Nominal / oldCurrency.Value; // Past currency value
        
        prices.each(function( index ) {
          currencies[index].textContent = code;
          $( this ).text(Math.round(calculation(val, oldVal, $( this ).text()) * 100) / 100);
        });
        $scope.book.price = Math.round(calculation(val, oldVal, $scope.book.price) * 100) / 100; // For $scope.changed
        $( '#subtotal' ).text(Math.round(calculation(val, oldVal, $( '#subtotal' ).text()) * 100) / 100);
        $( '#subtotalCurrency' ).text(code);
        $( '#subtotalCurrency' ).data( 'id', id ); // For formData
      }
    });

    $( '#orderForm' ).submit(function( event ) {
      if ($( '#Comment' ).val() == '') {
        $( '#Comment' ).val( " " );
      }
      var formData = {
        "manager": "semali1989@gmail.com",
        "book": $scope.bookId,
        "name": $( '#InputName' ).val(),
        "phone": $( '#InputPhone' ).val(),
        "email": $( '#InputEmail' ).val(),
        "comment": $( '#Comment' ).val(),
        "delivery": {
          "id": $scope.selectedDelivery.id,
          "address": $( '#deliveryAddress' ).val()
        },
        "payment": {
          "id": $scope.selectedPayment.id,
          "currency": $( '#subtotalCurrency' ).data( 'id' )
        }
      };
      var url = 'https://netology-fbb-store-api.herokuapp.com/order';
      $.ajax({
        type: "POST",
        url: url,
        data: formData,
        success: function(data) {
          $( '#orderForm' ).addClass( 'hidden' );
          $( '.successfully' ).removeClass( 'hidden' );
        },
        error: function(data) {
          console.log(data);
        }
      });
      event.preventDefault();
    });
    rerouting();
  }
]);

/* Redirect for search */
function rerouting() {
  $( '#search' ).bind( "keyup change input", function () {
    document.location.href='#/';
  });
}

/* Correct name of the currency */
function correctCurrencyName(arr) {
  var oldValue = $( '.currency' );
  oldValue.each(function( index ) {
    var span = $( this ).text();
    var objCurrency = arr.find(function(obj) {
      return obj.ID == span;
    });
    $( this ).text(objCurrency.CharCode);
    $( '#subtotalCurrency' ).text(objCurrency.CharCode);
  });
}

/* Cut out a substring */
function collectSubstring(a, b, c) {
  return a.substring(b, c);
}

/* Counting */
function calculation(value, from, to) {
  return value * to / from;
}

/* Tracking of the cursor */
function pupilMove() {
  $( window ).mousemove(function (event) {
    if ((window.location.href).indexOf('book') < 0) {
      return;
    }
    var centerEyeX= $( '.eye' ).offset().left + ($( '.eye' ).width() / 2), // X coordinate of the center of the eye
      centerEyeY= $( '.eye' ).offset().top + ($( '.eye' ).height() / 2), // Y coordinate of the center of the eye
      w1 = centerEyeX / 100, // 1 percent of the X axis at center
      h1 = centerEyeY / 100, // 1 percent of the Y axis at center
      MouseX = event.pageX, // Mouse position relative to the left edge of the document
      MouseY = event.pageY, // Mouse position relative to the top edge of the document
      windowWidth = $( window ).width(), // Width of the window
      windowHeight = $( window ).height(), // Height of the window
      radius = $( '.eye' ).width() / 2 - $( '#pupil' ).width() / 2, // Binding to radius of pupil
      rangeX = Math.abs((MouseX - centerEyeX) / w1),
      rangeY = Math.abs((MouseY - centerEyeY) / h1),
      left = radius / 100 * rangeX,
      top = radius / 100 * rangeY,
      a = Math.sqrt((top * top) + (left * left));
    if (MouseX < centerEyeX && MouseY < centerEyeY) {
      if ( a > radius) {
        var Mx = MouseX + centerEyeX;
        var My = MouseY + centerEyeY;
        var prop = Math.sqrt(((Mx * Mx) + (My * My))) / radius;
        top = Mx * (1 / prop);
        left = My * (1 / prop);
      }
      top = radius - top;
      left = radius - left;
    } else if (MouseX > centerEyeX && MouseY < centerEyeY) {
      if ( a > radius) {
        var Mx = MouseX - centerEyeX;
        var My = centerEyeY - MouseY;
        var prop = Math.sqrt(((Mx * Mx) + (My * My))) / radius;
        top = My * (1 / prop);
        left = Mx * (1 / prop);
      }
      top = radius - top;
      left = radius + left;
    } else if (MouseX > centerEyeX && MouseY > centerEyeY) {
      if ( a > radius) {
        var Mx = MouseX - centerEyeX;
        var My = MouseY - centerEyeY;
        var prop = Math.sqrt(((Mx * Mx) + (My * My))) / radius;
        top = My * (1 / prop);
        left = Mx * (1 / prop);
      }
      top = radius + top;
      left = radius + left;
    } else {
      if ( a > radius) {
        var Mx = centerEyeX - MouseX;
        var My = MouseY - centerEyeY;
        var prop = Math.sqrt(((Mx * Mx) + (My * My))) / radius;
        top = My * (1 / prop);
        left = Mx * (1 / prop);
      }
      top = radius + top;
      left = radius - left;
    }
    $( '#pupil' ).css({top:top,left:left});
  });
}

/* Eye blink */
function eyeBlink() {
  var blinkStages = 5;
  var i = 0;
  function blink () {
    setTimeout(function () {
      i++;
      if (i < blinkStages + 1) {
        $( '.eye' ).removeClass('blink' + (i - 1));
        $( '.eye' ).addClass('blink' + i);
        blink();
      } else {
        backBlink();
      }
    }, 30);
  }
  function backBlink () {
    setTimeout(function () {
      i--;
      if (i > 0) {
        $( '.eye' ).removeClass('blink' + i);
        $( '.eye' ).addClass('blink' + (i - 1));
        backBlink();
      }
    }, 30);
  }
  setInterval(function() {
    blink();
  }, 6000);
}

/* Drag and drop */
function dragAndDrop() {
  if ($( document ).width() <= 768) {
    return;
  }
  $( '.bookslist' ).sortable();
}